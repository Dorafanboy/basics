﻿using System;

namespace ExitControl
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string stopWord = "exit";

            while (true)
            {
                Console.WriteLine($"Введите любое слово или {stopWord} чтобы закончить цикл");
                var userInput = Console.ReadLine();

                if (userInput == stopWord)
                {
                    return;
                }
            }
        }
    }
}